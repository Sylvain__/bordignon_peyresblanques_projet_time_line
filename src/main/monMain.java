package main;

import java.util.ArrayList;

import graphique.Fenetre;
import methodes.MethodeDeck;
import methodes.MethodeRecupererDataCartes;
import methodes.MethodeRegleGenerale;
import methodes.MethodeRegleTimeline;
import metier.Deck;
import metier.Joueur;
import metier.Plateau;

public class monMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
MethodeRegleGenerale methoderegleregenerale = new MethodeRegleGenerale ();		
MethodeRegleTimeline methoderegletimeline = new MethodeRegleTimeline();			
MethodeDeck methoddeck = new MethodeDeck();	
MethodeRecupererDataCartes methoderecupererdatacartes = new MethodeRecupererDataCartes();
Deck deckMain = new Deck(null);
Plateau plateauMain =new Plateau(null);
ArrayList<Joueur> Joueurs= new ArrayList<>(); 
Joueur j1= new Joueur(1,3,"harryPotter",null);
Joueur j2= new Joueur(1,19,"carlo le calamar",null);
Joueur j3= new Joueur(1,19,"moulard",null);

Joueurs.add(j1);
Joueurs.add(j2);
Joueurs.add(j3);


		
deckMain = methoderecupererdatacartes.DataCartesTimeline();

System.out.println("-------------------------------------Deck melangé--------------------------------------------\n\n");
methoddeck.MelangerDeck(deckMain);


System.out.println("-------------------------------------Test distribution cartes à 2 joueurs max--------------------------------------------\n\n");

methoddeck.DistribuerCartesAuxJoueursRegleNormale(deckMain,Joueurs);

System.out.println(j1);
System.out.println(j2);
System.out.println(j3);


//gestion de l'interface
Fenetre fen= new Fenetre();

System.out.println("-------------------------------------Test Fonction retourne joueur le moins agé--------------------------------------------\n\n");

System.out.println(methoderegletimeline.RetourneIndexJoueurLePlusJeune(Joueurs));

System.out.println("-------------------------------------Test Fonction Ajout de la premiere carte sur le plateau--------------------------------------------\n\n");

methoderegleregenerale.DeposerPremiereCarteSurPlateau(deckMain, plateauMain);

System.out.println(plateauMain);

System.out.println("-------------------------------------Test Fonction Ajout de la carte du joueur j sur le plateau ( à la fin pour l'instant) --------------------------------------------\n\n");

//methoderegletimeline.PlacerCarteJoueurSurPlateau(j1, 2, plateauMain);

System.out.println("affichage des cartes du plateau : ");

System.out.println(plateauMain);

System.out.println("------------------------------------------------ Ajout d'une carte sur le plateau ------------------------------------------- ");

//methoderegletimeline.PlacerCarteJoueurSurPlateau(j1, 1, plateauMain, 0,true);
//methoderegletimeline.PlacerCarteJoueurSurPlateau(j2, 1, plateauMain, 0,true);


//methoderegletimeline.BoolPlacerCarteJoueurSurPlateauAuBonEndroit(j1, 1, plateauMain, 0,true);


if(methoderegletimeline.BoolPlacerCarteJoueurSurPlateauAuBonEndroit(j1, 1, plateauMain, 0,true)==false){
	
System.out.println("------------------------------------------------ Avant la suprresion de la mauvaise carte ------------------------------------------- ");
System.out.println(plateauMain);	
methoderegleregenerale.PiocherUneCarteEtSupprimerCelleDuPlateau(j1, deckMain, plateauMain, 0, true);	
System.out.println("------------------------------------------------ Après la suprresion de la mauvaise carte ------------------------------------------- ");	
System.out.println(plateauMain);
}
// methoderegletimeline.PlacerCarteJoueurSurPlateau(j2, 1, plateauMain, 0,false);
 //methoderegletimeline.PlacerCarteJoueurSurPlateau(j1, 1, plateauMain, 2,false);
 //methoderegletimeline.PlacerCarteJoueurSurPlateau(j1, 1, plateauMain, 1,false);
//methoderegletimeline.PlacerCarteJoueurSurPlateau(j2, 1, plateauMain, 0,false);

//methoderegletimeline.PlacerCarteJoueurSurPlateau(j1, 1, plateauMain, 0,true);
System.out.println(plateauMain);


	}

}
