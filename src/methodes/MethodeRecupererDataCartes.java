package methodes;

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import metier.Carte;
import metier.CarteCardLine;
import metier.CarteTimeLine;
import metier.Deck;


public class MethodeRecupererDataCartes {

	public Deck DataCartesTimeline(){

		ArrayList<Carte> listCartes = new ArrayList<>();	
		
		// parcours du fichier 
		String csvFile = "data/timeline/timeline.csv";
	    BufferedReader br = null;
	    String line = "";
	    String cvsSplitBy = ";";
	    int iterateur=1;
	    
	    try {

	        br = new BufferedReader(new FileReader(csvFile));
	        while ((line = br.readLine()) != null) {
	//System.out.println(iterateur);
	        	if (iterateur>1)
	        	{
	        		  //  on parcours et on stock les différentes instances de la classe Movie dans une liste contenant tous les movies
	        	      // split(..., -1) crée un String même si l'instance d'un objet est vide 
	                String[] tableCartes = line.split(cvsSplitBy,-1);
	            	            
	              
	            	  int dateCreation = Integer.parseInt(tableCartes[1]);
	                  
	            	   CarteTimeLine carte = new CarteTimeLine(tableCartes[0],dateCreation, tableCartes[2]);
	                   listCartes.add(carte);
	               
	        	}
	        	else{
	        	System.out.println("ce sont les titres ");	
	        	}
	        	iterateur++;
	          
	            
	        }

	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	  
	    Deck deckRempli=new Deck(listCartes);
	    System.out.println(deckRempli);
			return deckRempli;
		

	}	
	
	
	
	public Deck DataCartesCardline(){

		ArrayList<Carte> listCartes = new ArrayList<>();	
		
		// parcours du fichier 
		String csvFile = "data/cardline/cardline.csv";
	    BufferedReader br = null;
	    String line = "";
	    String cvsSplitBy = ";";
	    int iterateur=1;
	    
	    try {

	        br = new BufferedReader(new FileReader(csvFile));
	        while ((line = br.readLine()) != null) {
	//System.out.println(iterateur);
	        	if (iterateur>1)
	        	{
	        		  //  on parcours et on stock les différentes instances de la classe Movie dans une liste contenant tous les movies
	        	      // split(..., -1) crée un String même si l'instance d'un objet est vide 
	                String[] tableCartes = line.split(cvsSplitBy,-1);
	            
	             
	              
	            	//  int dateCreation = Integer.parseInt(tableCartes[1]);
	                int superficie = Integer.parseInt(tableCartes[1]);
	                int population = Integer.parseInt(tableCartes[2]);
	                int pib = Integer.parseInt(tableCartes[3]);
	                float pollution =Float.parseFloat(tableCartes[4]);
	            	  
	                  
	            	   CarteCardLine carte = new CarteCardLine(tableCartes[0],superficie,population,pib,pollution,tableCartes[6]);
	                   listCartes.add(carte);
	                 
	        	}
	        	else{
	        	System.out.println("ce sont les titres ");	
	        	}
	        	iterateur++;
	                   
	        }

	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	  
	   
	    Deck deckRempli=new Deck(listCartes);
	    System.out.println(deckRempli);
			return deckRempli;
		

	}		
	
	
}
