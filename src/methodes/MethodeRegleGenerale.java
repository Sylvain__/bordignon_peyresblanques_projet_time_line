package methodes;

import java.util.ArrayList;
import metier.Carte;
import metier.Deck;
import metier.Joueur;
import metier.Plateau;

public class MethodeRegleGenerale {

MethodeDeck methodedeck = new MethodeDeck();
	
public void DeposerPremiereCarteSurPlateau(Deck deck, Plateau plateau){
// vérifier si le deck contient bien l'ensemble des cartes 
// on selectionne la première carte de l'arrayList car on sait que le deck a été mélangé avant	
ArrayList<Carte> listCartes = new ArrayList<>();

listCartes.add(deck.getCartesDansDeck().get(0));
// pour pas que l'un des joueur se retrouve avec la carte déposé au début sur le plateau

deck.getCartesDansDeck().remove(0);
plateau.setCarteSurLaTable(listCartes);
	
}


public void PiocherUneCarteEtSupprimerCelleDuPlateau(Joueur j,Deck d,Plateau p,int indexEmplacementPlateau,boolean estAvant){

	if (estAvant == true){
	
	if(indexEmplacementPlateau==0){
		
		// suppression de la mauvaise carte sur le plateau	
		p.getCarteSurLaTable().remove(indexEmplacementPlateau);
		// faire piocher le joueur et enlever la carte pioché du deck 
		methodedeck.PiocherUneCarte(j, d);	
		
	}else{
		
		// suppression de la mauvaise carte sur le plateau	
		p.getCarteSurLaTable().remove(indexEmplacementPlateau-1);
		// faire piocher le joueur et enlever la carte pioché du deck 
		methodedeck.PiocherUneCarte(j, d);
		
	}

	
		
	}else{
		
	p.getCarteSurLaTable().remove(indexEmplacementPlateau+1);
	methodedeck.PiocherUneCarte(j, d);
			//
	}
	
	
}




}
