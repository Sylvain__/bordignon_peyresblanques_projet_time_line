package methodes;

import java.util.ArrayList;
import java.util.Collections;

import metier.Carte;
import metier.CarteTimeLine;
import metier.Joueur;
import metier.Plateau;

public class MethodeRegleTimeline {

MethodeDeck methodedeck = new MethodeDeck();	
	
public int RetourneIndexJoueurLePlusJeune(ArrayList<Joueur> joueurs){

int ageLemoinsGrand=121;
int indexDuJoueurLeMoinsGrand =-1;
ArrayList<Integer> ageMaxEgal = new ArrayList<>();

	for(int i=0;i< joueurs.size();i++){

	
	if (joueurs.get(i).getAge()==ageLemoinsGrand){

		ageMaxEgal.add(i);
		Collections.shuffle(ageMaxEgal);
	    indexDuJoueurLeMoinsGrand=ageMaxEgal.get(0);
			
			
		}
	else if(joueurs.get(i).getAge()<ageLemoinsGrand){
		
		ageLemoinsGrand=joueurs.get(i).getAge();
		indexDuJoueurLeMoinsGrand=i;	
		
	}
			
		}
	
	
if(indexDuJoueurLeMoinsGrand==-1 || indexDuJoueurLeMoinsGrand>=joueurs.size() ) 
throw new IllegalArgumentException("Erreur d'indexage du joueur le plus jeune");	

		
return indexDuJoueurLeMoinsGrand;	
}



public ArrayList<Joueur> DetermineOrdreDePassage(ArrayList<Joueur> joueurs ){
	int joueurLeMoinsGrand = this.RetourneIndexJoueurLePlusJeune(joueurs);
	ArrayList<Joueur> ordreFinalJoueurs = new ArrayList<>();
	// on ajoute dans la nouvelle liste d'abord le joueur le moins grand
	ordreFinalJoueurs.add(joueurs.get(joueurLeMoinsGrand));
	// on supprime de la liste d'entrée la valeur du joueur le plus petit 
	joueurs.remove(joueurLeMoinsGrand);

	
for(int i=0;i< joueurs.size();i++){
	
	ordreFinalJoueurs.add(joueurs.get(i));	
	
	
}

	return ordreFinalJoueurs;
}

// indexEmplacementPlateau = récupère l'emplacement de la carte qui a été cliqué sur le plateau , en fonction 
// de sa réponse à la question : voulez-vous la placer avant ou après l'index vaudra
// indexEmplacementPlateau-1 ou indexEmplacementPlateau+1






public boolean BoolPlacerCarteJoueurSurPlateauAuBonEndroit(Joueur J,int indexCarteChoisie,Plateau P,int indexEmplacementPlateau,boolean estAvant){
	// pour l'avoir il faut rajouter +1 à cette variable 
		int localisationCarteJoueurSurLePlateau=0;	
// insérer d'abord la carte du joueur à la fin du plateau 
int indexCarteDuJoueurSurPlateau; 
//P.getCarteSurLaTable().add(J.getCartesEnMain().get(indexCarteChoisie));
// peut être -1 ? 
indexCarteDuJoueurSurPlateau=P.getCarteSurLaTable().size();	

// on récupère d'abord les cartes du plateau avant l'insetion de la carte du joueur
ArrayList<Carte> carteActuelDuPlateauAvantInsertion = new ArrayList<>();
ArrayList<Carte> carteNouvelEtatDuPlateau = new ArrayList<>();

// créations des cartes a comparer ( si on doit faire piocher une carte ou non au joueur) 
CarteTimeLine carteJoueur = (CarteTimeLine) J.getCartesEnMain().get(indexCarteChoisie);





carteActuelDuPlateauAvantInsertion=P.getCarteSurLaTable();



// récupérer les cartes de l'ancien plateau avant ou egale à l'index indexEmplacementPlateau

// au cas ou si l'index est égale à 0 

//au cas ou si l'index est  -1 
//else if( indexEmplacementPlateau ==-1){
 if( indexEmplacementPlateau ==0 && estAvant==true){
carteNouvelEtatDuPlateau.add(J.getCartesEnMain().get(indexCarteChoisie));

for (int i=0;i<P.getCarteSurLaTable().size();i++){

carteNouvelEtatDuPlateau.add(carteActuelDuPlateauAvantInsertion.get(i)); 	
		
	}
P.getCarteSurLaTable().clear();
P.getCarteSurLaTable().addAll(carteNouvelEtatDuPlateau);
// supprimer la carte du joueur 
J.getCartesEnMain().remove(indexCarteChoisie);


	
	
	
}
else if (estAvant==true){
	

for (int i=0;i<indexEmplacementPlateau;i++){
carteNouvelEtatDuPlateau.add(carteActuelDuPlateauAvantInsertion.get(i)); 		
	
}
carteNouvelEtatDuPlateau.add(J.getCartesEnMain().get(indexCarteChoisie));
System.out.println("LA CARTE DU JOUEUR AJOUTE AU PLATEAU EST : ");
System.out.println(J.getCartesEnMain().get(indexCarteChoisie));

System.out.println("NB CARTE PLATEAU AVANT INSERTION :");
System.out.println(carteActuelDuPlateauAvantInsertion.size());

for (int i=indexEmplacementPlateau;i<carteActuelDuPlateauAvantInsertion.size();i++){
	
carteNouvelEtatDuPlateau.add(carteActuelDuPlateauAvantInsertion.get(i)); 		
	
}
	
P.getCarteSurLaTable().clear();
P.getCarteSurLaTable().addAll(carteNouvelEtatDuPlateau);
//supprimer la carte du joueur 
J.getCartesEnMain().remove(indexCarteChoisie);

	
}
 // Si après
else{
	

	for (int i=0;i<=indexEmplacementPlateau;i++){
			
		carteNouvelEtatDuPlateau.add(carteActuelDuPlateauAvantInsertion.get(i)); 		
		localisationCarteJoueurSurLePlateau++;
		}
	carteNouvelEtatDuPlateau.add(J.getCartesEnMain().get(indexCarteChoisie));


	System.out.println(carteActuelDuPlateauAvantInsertion.size());
	
	for (int i =indexEmplacementPlateau+1;i<carteActuelDuPlateauAvantInsertion.size();i++){
		System.out.println("Je passe ici ??");
		carteNouvelEtatDuPlateau.add(carteActuelDuPlateauAvantInsertion.get(i)); 		

		}
	
	
	P.getCarteSurLaTable().clear();
	P.getCarteSurLaTable().addAll(carteNouvelEtatDuPlateau);

	//supprimer la carte du joueur 
	J.getCartesEnMain().remove(indexCarteChoisie);

	
	
	
}

// comparer la date de la carte du joueur avec celle du plateau 
if(estAvant==true){
System.out.println("ETAT DU PLATEAU POUR COMPARER CARTE");
System.out.println(P.getCarteSurLaTable());

CarteTimeLine cartePlateau = (CarteTimeLine) P.getCarteSurLaTable().get(indexEmplacementPlateau+1);

if (carteJoueur.getDate()<cartePlateau.getDate()){
	// on ne fait pas piocher de carte au joueur 
	System.out.println("Carte date joueur : "+carteJoueur.getDate());
	System.out.println("Carte date plateau : "+cartePlateau.getDate());
	System.out.println("tu ne pioche pas de carte !!!");
	
	
	return true;
}else{
	System.out.println("Carte date joueur : "+carteJoueur.getDate());
	System.out.println("Carte date plateau : "+cartePlateau.getDate());
	
	// Supprimer la carte du plateau qui n'est pas bonne
	
	
	// pioche 
	System.out.println("tu  pioche  une carte !!!");
	P.getCarteSurLaTable().remove(localisationCarteJoueurSurLePlateau);
	
	
	//methodedeck.PiocherUneCarte(J, d);
	
	return false;
	
}

	
}else{
CarteTimeLine cartePlateau = (CarteTimeLine) P.getCarteSurLaTable().get(indexEmplacementPlateau);	


if (carteJoueur.getDate()>cartePlateau.getDate()){
	
	System.out.println("tu ne pioche pas de carte !!!");
	// on ne fait pas piocher de carte au joueur 
	return true;
}else{
	// supprimer la mauvaise carte placé sur le plateau 
	P.getCarteSurLaTable().remove(localisationCarteJoueurSurLePlateau);
	
	// pioche 	
	
	System.out.println("tu  pioche  une carte !!!");
	
  return false;
}
	
	
}




	
}





public int ComparerCarteJoueurAvecCartePlateau(Joueur J,int indexCarteChoisie,Plateau P, int indexEmplacementPlateau){
// Sauvegarder dans carte la carte a tranférer 
CarteTimeLine carteJoueur;
CarteTimeLine cartePlateau;
ArrayList<Carte> cartesDuPlateauDeCarte = new ArrayList<>();

carteJoueur=(CarteTimeLine) J.getCartesEnMain().get(indexCarteChoisie);
// supprimer de sa main la carte qu'il a sélectionné 
J.getCartesEnMain().remove(indexCarteChoisie);

// récupérer les cartes du plateau
cartesDuPlateauDeCarte=P.getCarteSurLaTable();
//récupérer la carte séléctionné du plateau 
cartePlateau=(CarteTimeLine) cartesDuPlateauDeCarte.get(indexEmplacementPlateau);

// comparer les dates des cartes entre celle du joueur et celle du plateau : 







return 0;

}





	
}
