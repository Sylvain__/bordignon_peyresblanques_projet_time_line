package metier;

public abstract class Carte {
private String nom,nom_de_l_image;


public Carte(String nom, String nom_de_l_image) {
	super();
	this.nom = nom;
	this.nom_de_l_image = nom_de_l_image;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}


public String getNom_de_l_image() {
	return nom_de_l_image;
}

public void setNom_de_l_image(String nom_de_l_image) {
	this.nom_de_l_image = nom_de_l_image;
}

@Override
public String toString() {
	return "Carte [nom=" + nom + ", nom_de_l_image=" + nom_de_l_image + "]";
}


@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nom == null) ? 0 : nom.hashCode());
	result = prime * result + ((nom_de_l_image == null) ? 0 : nom_de_l_image.hashCode());
	return result;
}



@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Carte other = (Carte) obj;
	if (nom == null) {
		if (other.nom != null)
			return false;
	} else if (!nom.equals(other.nom))
		return false;
	if (nom_de_l_image == null) {
		if (other.nom_de_l_image != null)
			return false;
	} else if (!nom_de_l_image.equals(other.nom_de_l_image))
		return false;
	return true;
}
	

}
