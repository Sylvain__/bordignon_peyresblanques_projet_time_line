package metier;

public class CarteTimeLine extends Carte {
private int date;



public CarteTimeLine(String nom, int date, String nom_de_l_image) {
	super(nom,nom_de_l_image);
	this.date = date;
}


public int getDate() {
	return date;
}


public void setDate(int date) {
	this.date = date;
}





@Override
public String toString() {
	return "CarteTimeLine [date=" + date + ", nom=" + getNom() + ", nomImage=" + getNom_de_l_image()
			+"]\n";
}


@Override
public int hashCode() {
	final int prime = 31;
	int result = super.hashCode();
	result = prime * result + date;
	return result;
}


@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (!super.equals(obj))
		return false;
	if (getClass() != obj.getClass())
		return false;
	CarteTimeLine other = (CarteTimeLine) obj;
	if (date != other.date)
		return false;
	return true;
}


}
