package metier;

import java.util.ArrayList;

public class Deck {
	
	private ArrayList<Carte> cartesDansDeck;
	
	
	public Deck(ArrayList<Carte> cartesDansDeck) {
		super();
		this.cartesDansDeck = cartesDansDeck;
	}
	
	
	

	public ArrayList<Carte> getCartesDansDeck() {
		return cartesDansDeck;
	}

	public void setCartesDansDeck(ArrayList<Carte> cartesDansDeck) {
		this.cartesDansDeck = cartesDansDeck;
	}

	@Override
	public String toString() {
		return "Deck [cartesDansDeck=" + cartesDansDeck + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cartesDansDeck == null) ? 0 : cartesDansDeck.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deck other = (Deck) obj;
		if (cartesDansDeck == null) {
			if (other.cartesDansDeck != null)
				return false;
		} else if (!cartesDansDeck.equals(other.cartesDansDeck))
			return false;
		return true;
	}
	
	

}
