package metier;

import java.util.ArrayList;

public class Joueur {
 	
private int age,id ;
private String name; 
private ArrayList<Carte> cartesEnMain;



public Joueur(int id, int age, String name,ArrayList<Carte> cartesEnMain) {
if(age<3  || age > 120)throw new IllegalArgumentException("veuillez saisir un age correcte entre 3 et 100 ans afin de pouvoir débuter la partie");
	this.age = age;
	this.id = id;
	this.name = name;
	this.cartesEnMain = cartesEnMain ;
}




@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + age;
	result = prime * result + ((cartesEnMain == null) ? 0 : cartesEnMain.hashCode());
	result = prime * result + id;
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Joueur other = (Joueur) obj;
	if (age != other.age)
		return false;
	if (cartesEnMain == null) {
		if (other.cartesEnMain != null)
			return false;
	} else if (!cartesEnMain.equals(other.cartesEnMain))
		return false;
	if (id != other.id)
		return false;
	if (name == null) {
		if (other.name != null)
			return false;
	} else if (!name.equals(other.name))
		return false;
	return true;
}











@Override
public String toString() {
	return "Joueur [age=" + age + ", id=" + id + ", name=" + name + ", cartesEnMain=" + cartesEnMain + "]";
}


public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public ArrayList<Carte> getCartesEnMain() {
	return cartesEnMain;
}
public void setCartesEnMain(ArrayList<Carte> cartesEnMain) {
	this.cartesEnMain = cartesEnMain;
}





}
