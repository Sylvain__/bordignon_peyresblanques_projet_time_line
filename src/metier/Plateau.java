package metier;

import java.util.ArrayList;
import metier.Carte;
public class Plateau {
	
private ArrayList<Carte> carteSurLaTable;

public Plateau(ArrayList<Carte> carteSurLaTable) {
	super();
	this.carteSurLaTable = carteSurLaTable;
}




public ArrayList<Carte> getCarteSurLaTable() {
	return carteSurLaTable;
}

public void setCarteSurLaTable(ArrayList<Carte> carteSurLaTable) {
	this.carteSurLaTable = carteSurLaTable;
}

@Override
public String toString() {
	return "Plateau [carteSurLaTable=" + carteSurLaTable + "]";
}


@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((carteSurLaTable == null) ? 0 : carteSurLaTable.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Plateau other = (Plateau) obj;
	if (carteSurLaTable == null) {
		if (other.carteSurLaTable != null)
			return false;
	} else if (!carteSurLaTable.equals(other.carteSurLaTable))
		return false;
	return true;
} 





}
