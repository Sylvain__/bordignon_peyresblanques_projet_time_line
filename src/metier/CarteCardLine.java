package metier;

public class CarteCardLine extends Carte {
private int superficie,population,pib;
private float polution;


public CarteCardLine(String nomPays, int superficie, int population, int pib, float polution,String nom_de_l_image ) {

	super(nomPays,nom_de_l_image);
	this.superficie = superficie;
	this.population = population;
	this.pib = pib;
	this.polution = polution;
}


public int getSuperficie() {
	return superficie;
}


public void setSuperficie(int superficie) {
	this.superficie = superficie;
}


public int getPopulation() {
	return population;
}


public void setPopulation(int population) {
	this.population = population;
}


public int getPib() {
	return pib;
}


public void setPib(int pib) {
	this.pib = pib;
}


public float getPolution() {
	return polution;
}


public void setPolution(float polution) {
	this.polution = polution;
}



@Override
public String toString() {
	return "CarteCardLine [superficie=" + superficie + ", population=" + population + ", pib=" + pib + ", polution="
			+ polution + ", getNom()=" + getNom() + ", getNom_de_l_image()=" + getNom_de_l_image() + ", toString()="
			+ super.toString() + ", getClass()=" + getClass() + "]\n";
}


@Override
public int hashCode() {
	final int prime = 31;
	int result = super.hashCode();
	result = prime * result + pib;
	result = prime * result + Float.floatToIntBits(polution);
	result = prime * result + population;
	result = prime * result + superficie;
	return result;
}


@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (!super.equals(obj))
		return false;
	if (getClass() != obj.getClass())
		return false;
	CarteCardLine other = (CarteCardLine) obj;
	if (pib != other.pib)
		return false;
	if (Float.floatToIntBits(polution) != Float.floatToIntBits(other.polution))
		return false;
	if (population != other.population)
		return false;
	if (superficie != other.superficie)
		return false;
	return true;
}








	
}
