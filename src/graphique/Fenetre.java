package graphique;

import java.awt.GridLayout; 
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;
import methodes.MethodeDeck;
import methodes.MethodeRecupererDataCartes;
import methodes.MethodeRegleGenerale;
import methodes.MethodeRegleTimeline;
import metier.Deck;
import metier.Joueur;

import java.awt.Color; 

import java.awt.Graphics;
 
public class Fenetre extends JFrame {

  public Fenetre(){	  
    this.setTitle("A quel jeu souhaitez vous jouer ?");
    this.setSize(400, 500);
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
    JPanel panel = new JPanel();
	panel.setBackground(Color.GRAY);
	this.setContentPane(panel);
	// Demander nombre joueurs
	this.DemanderInformationDesJoueurs();
	
    //On définit le layout à utiliser sur le content panel
    //Trois lignes sur deux colonnes
    this.setLayout(new GridLayout(2,1));
    //On ajoute le bouton au content panel de la JFrame
    JButton a= new JButton();
    a.setIcon(new ImageIcon("data/LogoTimeline.png"));
    this.getContentPane().add(a);
    //Lors du clic sur le bouton Timeline
    a.addActionListener(new ActionListener(){  
    	public void actionPerformed(ActionEvent e){
    	ExecuterTimeline(); 
    	        }  
    }); 
    
    
   
    //Redimensionnement de l'image pour qu'elle ne s'affiche pas coupée sur l'écran
    ImageIcon iconCardline = new ImageIcon(new ImageIcon("data/logoCardline.png").getImage().getScaledInstance(400, 300, java.awt.Image.SCALE_SMOOTH));
    JButton b = new JButton();
    b.setIcon(iconCardline);
    this.getContentPane().add(b);
    //Lors du clic sur le bouton Cardline
    b.addActionListener(new ActionListener(){  
    	public void actionPerformed(ActionEvent e){  
    		ExecuterCardline(); 
    	        }  
    }); 

    this.setVisible(true);
    
        
  }
  
  
  
  MethodeRegleGenerale methoderegleregenerale = new MethodeRegleGenerale ();		
  MethodeRegleTimeline methoderegletimeline = new MethodeRegleTimeline();			
  MethodeDeck methoddeck = new MethodeDeck();	
  MethodeRecupererDataCartes methoderecupererdatacartes = new MethodeRecupererDataCartes();
  ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
  
  //fonction qui permet d'executer le plateau de timeline
  public void ExecuterTimeline(){

	  TimeLineFen time = new TimeLineFen(joueurs);
		

  }
  
  //fonction qui permet d'executer le plateau de cardline
  public void ExecuterCardline(){
	 
	  CardlineFenetre card = new CardlineFenetre();
  }
  
  
   JPanel zoneDessin;
   
  
  int nombreJoueur;


  
  public void DemanderInformationDesJoueurs()
  {
      // ImageIcon iconNameJoueur = new ImageIcon("data/personneLogo.png");
      ImageIcon iconNameGroupe = new ImageIcon("data/groupeLogo.png");

      //int nombre = Integer.parseInt((String) JOptionPane.showInputDialog(zoneDessin, "Combien de joueurs êtes vous ? (max 8)", "", JOptionPane.WARNING_MESSAGE, iconNameGroupe , null, null));
      String nombreString = (String) JOptionPane.showInputDialog(zoneDessin, "Combien de joueurs êtes vous ? (max 8)", "", JOptionPane.WARNING_MESSAGE, iconNameGroupe , null, null);
      //On vérifie que l'entrée ne soit pas vide
      if (nombreString.isEmpty())
      {
    	  DemanderInformationDesJoueurs();
      }
    else
      {
    	
    	  int nombre = Integer.parseInt(nombreString);
	      if (nombre == 0)
	      {
	    	  DemanderInformationDesJoueurs();
	   
	      }
	      if (nombre > 8)
	      {
	    	  DemanderInformationDesJoueurs();
	      } 
	      else 
	      {
	         nombreJoueur = nombre;
	         
	         for(int i=1;i<=nombreJoueur;i++){
	        	 String nom = (String) JOptionPane.showInputDialog(zoneDessin, "Saisir le pseudo du joueur "+i, "", JOptionPane.WARNING_MESSAGE, iconNameGroupe , null, null);
	        	 String age = (String) JOptionPane.showInputDialog(zoneDessin, "Veuillez saisir l'age du joueur"+i, "", JOptionPane.WARNING_MESSAGE, iconNameGroupe , null, null); 
	         
	        // verifier si l'age rentrée est bien un nombre
	        // fait appel a la fonction estEntier qui vérifie si la sortie du String est bien un nombre
	        	 
	        boolean    verifPseudo = pseudoEstCorrecte(nom);
	        boolean    verifEntier = estEntier(age);
	   
	        
	        while(verifPseudo == false){
		        JOptionPane.showMessageDialog(zoneDessin, "Le pseudo saisi doit comprendre au moins 1 caractère et maximum 15");	
		        nom = (String) JOptionPane.showInputDialog(zoneDessin, " Veuillez resaisir le pseudo du joueur"+i, "", JOptionPane.WARNING_MESSAGE, iconNameGroupe , null, null);
		        verifPseudo = pseudoEstCorrecte(nom);	
		        	    	        	 	
		        }
	        
	        
	        while(verifEntier == false){
	        JOptionPane.showMessageDialog(zoneDessin, "Age incorecte ! \n\n(note : il faut rentrer uniquement des entiers et l'age doit être supérieure à 3 ans.)");	
	        age = (String) JOptionPane.showInputDialog(zoneDessin, "Veuillez resaisir l'age du joueur"+i, "", JOptionPane.WARNING_MESSAGE, iconNameGroupe , null, null);	
	        verifEntier = estEntier(age);	
	        	
	 
	    	        	 	
	        }
	        
	        	   Joueur joueur = new Joueur(i,Integer.parseInt(age),nom,null) ;
		           joueurs.add(joueur);
		                 
	        	 }
	         
	      }
      }
      
    
}
  
  public  boolean estEntier(String in){
	    boolean parsable = true;	  
	    try{
	        Integer.parseInt(in);
	    }catch(NumberFormatException e){
	      
	        return false;
	    }
	    
	    int age =   Integer.parseInt(in);
	    if(age<3){
	    parsable = false;
	    }
	    
	    return parsable;
	}
  
  public  boolean pseudoEstCorrecte(String in){
	  boolean check=true;
	 if(in.length()==0 || in.length()>15) {
		 
	check=false;	 
	 }
	 else{
   check=true;	 	 
	 }
	 
	  return check;
	}
  
  

	  
	  
	  
	  
	
	}
  
  





