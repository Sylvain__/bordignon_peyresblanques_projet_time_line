package graphique;
import javax.swing.JFrame;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import metier.Deck;
import metier.Joueur;
import metier.Plateau;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import methodes.MethodeDeck;
import methodes.MethodeRecupererDataCartes;
import methodes.MethodeRegleGenerale;
import methodes.MethodeRegleTimeline;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class TimeLineFen extends JFrame implements ActionListener {


	
	JLabel lbl_nom_carte_joueur_selectionne = new JLabel("vide");
	JLabel lbl_nom_carte_plateau_selectionne = new JLabel("vide");
	JLabel lbl_score_j0 = new JLabel();
	JLabel lbl_score_j1 = new JLabel();
	JLabel lbl_score_j2 = new JLabel();
	JLabel lbl_score_j3 = new JLabel();
	JLabel lbl_score_j4 = new JLabel();
	JLabel lbl_score_j5 = new JLabel();
	JLabel lbl_score_j6 = new JLabel();
	JLabel lbl_score_j7 = new JLabel();
 JLabel lbl_nom_carte_clique_plateau = new JLabel("Nom de la carte cliqué sur le plateau : ");
	
	JLabel lbl_nom_joueur_en_cours = new JLabel("__");
	JPanel panel_titre = new JPanel();
	JPanel pnl_carte_jeu = new JPanel();
	JPanel pnl_deck = new JPanel();
	JPanel pnl_cartes_table = new JPanel();
	JPanel pnl_cartes_joueur = new JPanel();
	JButton btn_cartes_du_joueur[] = new JButton[6];
	ImageIcon img_cartes_du_joueur[]=new ImageIcon [6];
	Image imga_cartes_du_joueur[]=new Image [6];
	Image imgnew_cartes_du_joueur[]=new Image [6];
	int test=0;
	JButton btn_cartes_du_plateau[] = new JButton[20];
	ImageIcon img_cartes_du_plateau[]=new ImageIcon [20];
	Image imga_cartes_du_plateau[]=new Image [20];
	Image imgnew_cartes_du_plateau[]=new Image [20];
	
	
	JPanel pnl_score = new JPanel();
//	JScrollPane scrollPane = new JScrollPane(pnl_carte_jeu);
	Plateau plateauMain =new Plateau(null);
	Deck deckMain = new Deck(null);
	ArrayList<Joueur> listesDesJoueurs = new ArrayList<Joueur>();
	int changerJoueur=0;
	int numeroDuTour=1;
	// test scroll

	
	 
	JLabel lbl_numro_du_tour = new JLabel("Numéro du tour : "+numeroDuTour);
	//methodes utilisées sur la fenetre TimeLineFen
	MethodeRegleGenerale methoderegleregenerale = new MethodeRegleGenerale ();		
	MethodeRegleTimeline methoderegletimeline = new MethodeRegleTimeline();			
	MethodeDeck methoddeck = new MethodeDeck();	
	MethodeRecupererDataCartes methoderecupererdatacartes = new MethodeRecupererDataCartes();
	
	//------------------------------------------------
	

	
	public TimeLineFen( ArrayList<Joueur> joueurs) {

	// on mélange l'ordre de passage en respectant la regle du plus jeune qui commence
			
		listesDesJoueurs=DetermineOrdreDePassage(joueurs);	
		
	// on charge les cartes	
	deckMain = methoderecupererdatacartes.DataCartesTimeline();
	// on mélange les cartes 
	methoddeck.MelangerDeck(deckMain);
	
	// dépôt de la premiere carte sur le terrain de jeu 
	DeposerPremiereCarteSurPlateau(deckMain,plateauMain);
	
		
			
//		JButton j2 = new JButton();
//		j2.setBackground(new Color(0, 0, 0));
//		j2.setBounds(195, 98, 120, 180);
//		pnl_deck.add(j2);
		
//		JButton j3 = new JButton();
//		j3.setBackground(new Color(0, 0, 0));
//		j3.setBounds(327, 98, 120, 180);
//		pnl_deck.add(j3);
	
	
//	JButton j = new JButton();
//	j.setBounds(23, 76, 120, 180);
//	pnl_cartes_joueur.add(j);	
		
	getContentPane().setLayout(null);
		 this.setSize(1000, 916);
		
		panel_titre.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panel_titre.setBounds(6, 6, 939, 31);
		getContentPane().add(panel_titre, BorderLayout.WEST);
		panel_titre.setLayout(null);
		
		JLabel lbl_titre_jeu = new JLabel("Bienvenue sur TimeLine");
		lbl_titre_jeu.setBounds(320, 0, 155, 31);
		panel_titre.add(lbl_titre_jeu);
		
		
		pnl_carte_jeu.setBounds(6, 211, 939, 388);
		getContentPane().add(pnl_carte_jeu);
		pnl_carte_jeu.setLayout(null);
		
		
		
		pnl_deck.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		pnl_deck.setBounds(179, 0, 760, 388);
	
		pnl_carte_jeu.add(pnl_deck);
		pnl_deck.setLayout(null);
		
		
		JLabel lbl_titre_plateau = new JLabel("Carte du Plateau");
		lbl_titre_plateau.setBounds(235, 6, 108, 16);
		pnl_deck.add(lbl_titre_plateau);
		lbl_nom_carte_clique_plateau.setBounds(6, 351, 669, 16);
		
		pnl_deck.add(lbl_nom_carte_clique_plateau);
		
		// test ajout JscrollPane
		
	//	scrollPane.setBounds(24, 79, 545, 182);	
	//	pnl_deck.add(scrollPane);
	
		pnl_cartes_table.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		pnl_cartes_table.setLayout(null);
		pnl_cartes_table.setBounds(0, 0, 173, 388);
		pnl_carte_jeu.add(pnl_cartes_table);
	
		// ajustement de l'image deck
		ImageIcon imagedeck = new ImageIcon("data/timeline/cards/dos_carte.png"); 
		Image image = imagedeck .getImage(); // transform it 
		Image newimg = image.getScaledInstance(120, 180,  java.awt.Image.SCALE_SMOOTH); // 
		imagedeck  = new ImageIcon(newimg);  // 
		JLabel lbl_img_deck = new JLabel("");
		lbl_img_deck.setIcon(imagedeck);
		
		lbl_img_deck.setBounds(20, 49, 147, 270);
		pnl_cartes_table.add(lbl_img_deck);
		
		JLabel lbl_titre_deck = new JLabel("Cartes du Deck");
		lbl_titre_deck.setBounds(38, 6, 96, 16);
		pnl_cartes_table.add(lbl_titre_deck);
		
		pnl_cartes_joueur.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		pnl_cartes_joueur.setBounds(6, 611, 939, 277);
		getContentPane().add(pnl_cartes_joueur);
		pnl_cartes_joueur.setLayout(null);
		
		JLabel lbl_titre_carte_joueur = new JLabel("Carte Main Joueur");
		lbl_titre_carte_joueur.setBounds(335, 6, 116, 16);
		pnl_cartes_joueur.add(lbl_titre_carte_joueur);
		
		
		lbl_nom_joueur_en_cours.setBounds(6, 24, 298, 16);
		pnl_cartes_joueur.add(lbl_nom_joueur_en_cours);
		
		
		pnl_score.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		pnl_score.setBounds(6, 49, 939, 161);
		getContentPane().add(pnl_score);
		pnl_score.setLayout(null);
		
		JLabel lblScoreDeLa = new JLabel("Score de la partie :");
		lblScoreDeLa.setBounds(335, 6, 116, 16);
		pnl_score.add(lblScoreDeLa);
		
	
		lbl_score_j0.setBounds(21, 69, 96, 16);
		pnl_score.add(lbl_score_j0);
		
	
		lbl_score_j1.setBounds(21, 110, 96, 16);
		pnl_score.add(lbl_score_j1);
		
		
		lbl_score_j2.setBounds(179, 69, 96, 16);
		pnl_score.add(lbl_score_j2);
		
		
		lbl_score_j3.setBounds(179, 110, 96, 16);
		pnl_score.add(lbl_score_j3);
		
		
		lbl_score_j4.setBounds(390, 69, 96, 16);
		pnl_score.add(lbl_score_j4);
		
	
		lbl_score_j5.setBounds(390, 110, 96, 16);
		pnl_score.add(lbl_score_j5);
		

		lbl_score_j6.setBounds(615, 69, 96, 16);
		pnl_score.add(lbl_score_j6);
		
		lbl_score_j7.setBounds(615, 110, 96, 16);
		pnl_score.add(lbl_score_j7);
		
	
		lbl_numro_du_tour.setBounds(335, 139, 151, 16);
		pnl_score.add(lbl_numro_du_tour);
	
	
	   this.setVisible(true);
	   pnl_cartes_table.setVisible(true);
		
		AfficheListeJoueurs(listesDesJoueurs);
		AfficheListeJoueursSurScores(listesDesJoueurs);
		AfficheLePremierJoueurAJouer(listesDesJoueurs);
		DistribuerCartesAuxJoueursRegleNormale(deckMain,listesDesJoueurs);
		//????
		AffichePremiereCarteSurleTerrain2(plateauMain);
		
		AfficheCartesDuJoueurX(listesDesJoueurs.get(changerJoueur));
	
	 System.out.println("---------CONTENU DU PLATEAU-----------");
	 System.out.println(plateauMain);
	 System.out.println("---------JOUEURS-----------");
	 System.out.println(listesDesJoueurs);
	 
	 
					
	}
	
	// ------------------------------------------fonctions de la fenetre---------------------------------------------
	
	
public void DistribuerCartesAuxJoueursRegleNormale(Deck deck,ArrayList<Joueur> listesjoueurs){
// 1- determiner cb de joueur sont dans la partie 
	methoddeck.DistribuerCartesAuxJoueursRegleNormale(deck, listesjoueurs);
	
	}
// pour pouvoir rafraichir les cartes en main 	
public void SupprimerCartesDuJoueur(){
	
	for(int i=0;i<btn_cartes_du_joueur.length;i++){
//	System.out.println("taille du tableau du bouton : "+btn_cartes_du_joueur.length);
		pnl_cartes_joueur.remove(btn_cartes_du_joueur[i]);	
		pnl_cartes_joueur.revalidate();
		pnl_cartes_joueur.repaint();
	}
	
	
}

public void SupprimerCartesDuPlateau(){


	for(int i=0;i<test;i++){
		

		
	

	System.out.println(plateauMain.getCarteSurLaTable());
		// Pourquoi ERROR?????
	    pnl_deck.remove(btn_cartes_du_plateau[i]);
		pnl_deck.revalidate();
		pnl_deck.repaint();
		}
	
	
	
}



public void AfficheCartesDuJoueurX(Joueur j){

// définir l'emplacement X de la premiere carte dans la main du joueur
//	JButton jc = new JButton();
//	jc.setBounds(23, 76, 120, 180);
//	pnl_cartes_joueur.add(jc);	
	
int x,y,longueur,hauteur;
String chemin;
chemin="data/timeline/cards/";
x=23;
y=76;
longueur=120;
hauteur=180;
Image c0;

	
for(int i=0;i<j.getCartesEnMain().size();i++){

img_cartes_du_joueur[i]=new ImageIcon(chemin+j.getCartesEnMain().get(i).getNom_de_l_image()+".jpeg");
imga_cartes_du_joueur[i]= img_cartes_du_joueur[i].getImage();
c0=imga_cartes_du_joueur[i].getScaledInstance(120, 180,  java.awt.Image.SCALE_SMOOTH);
img_cartes_du_joueur[i]=new ImageIcon(c0);
btn_cartes_du_joueur[i] = new JButton(""+i);
btn_cartes_du_joueur[i].setIcon(img_cartes_du_joueur[i]);
btn_cartes_du_joueur[i].setBounds(x, y, longueur, hauteur);
// ajout d'un action listener sur les boutons 
btn_cartes_du_joueur[i].addActionListener(this);
pnl_cartes_joueur.add(btn_cartes_du_joueur[i]);


x=x+140;	
}
		
}


public void AffichePremiereCarteSurleTerrain2(Plateau p){
	
int x,y,longueur,hauteur;
String chemin;
chemin="data/timeline/cards/";
x=49;
y=98;
longueur=120;
hauteur=180;
Image c0;

	
for(int i=0;i<p.getCarteSurLaTable().size();i++){
System.out.println("---IIIICCCCCCCCIIIIIIIII--");
System.out.println(chemin+p.getCarteSurLaTable().get(i).getNom_de_l_image()+".jpeg");
img_cartes_du_plateau[i]=new ImageIcon(chemin+p.getCarteSurLaTable().get(i).getNom_de_l_image()+".jpeg");
imga_cartes_du_plateau[i]= img_cartes_du_plateau[i].getImage();
c0=imga_cartes_du_plateau[i].getScaledInstance(120, 180,  java.awt.Image.SCALE_SMOOTH);
img_cartes_du_plateau[i]=new ImageIcon(c0);

pnl_deck.revalidate();
pnl_deck.repaint();

// ???? peut etre bug car id carte plateau = id carte joueur ( mais pas ds meme panel donc peut etre ok)
btn_cartes_du_plateau[i] = new JButton("p"+i);
btn_cartes_du_plateau[i].setIcon(img_cartes_du_plateau[i]);
btn_cartes_du_plateau[i].setBounds(x, y, longueur, hauteur);
// ajout d'un action listener sur les boutons 
btn_cartes_du_plateau[i].addActionListener(this);
pnl_deck.add(btn_cartes_du_plateau[i]);



System.out.println("NNNNNNNNNNNOMMMMMBRE CARTE PLATEAU : "+p.getCarteSurLaTable().size());
x=x+140;

}


}




	public void AffichePremiereCarteSurleTerrain(){
		
		ImageIcon imageCarte1Jeu = new ImageIcon("data/timeline/cards/"+plateauMain.getCarteSurLaTable().get(0).getNom_de_l_image()+".jpeg"); 
		Image imgCarte1Jeu = imageCarte1Jeu .getImage(); 
		Image newimgCarte1Jeu = imgCarte1Jeu.getScaledInstance(120, 180,  java.awt.Image.SCALE_SMOOTH);
		imageCarte1Jeu  = new ImageIcon(newimgCarte1Jeu);
		//le nom   ????
		JButton j1 = new JButton("0");
		j1.setIcon(imageCarte1Jeu);
		j1.setBounds(49, 98, 120, 180);
		j1.addActionListener(this);
		pnl_deck.add(j1);	
		
	}
	
	public void DeposerPremiereCarteSurPlateau(Deck deck, Plateau plateau){
 
		methoderegleregenerale.DeposerPremiereCarteSurPlateau(deck, plateau);
			
	}
	
	public  ArrayList<Joueur> DetermineOrdreDePassage(ArrayList<Joueur> joueurs){
	return methoderegletimeline.DetermineOrdreDePassage(joueurs);	
	
	}
		
	  public  void AfficheListeJoueurs( ArrayList<Joueur> joueurs){
		  System.out.println("-----------LISTE JOUEURS---------------------------");	
		  for(int i=0;i<joueurs.size();i++ ){
		System.out.println(joueurs.get(i));	
		System.out.println(i);
		  }	  	  
	}
  
	  public int NombreJoueursDansListe(ArrayList<Joueur> joueurs){	  
		return joueurs.size();  	  
	  }
	  
	  
	  public void AfficheLePremierJoueurAJouer( ArrayList<Joueur> joueurs){
	// on determine le joueur le plus jeune 
		  int indexJoueurLePlusJeune;
		  indexJoueurLePlusJeune = methoderegletimeline.RetourneIndexJoueurLePlusJeune(joueurs);
		  lbl_nom_joueur_en_cours.setText("Au tour du joueur "+joueurs.get(indexJoueurLePlusJeune).getName().toString()+" de jouer !");
		  	  
	  }
	  
	  public  void AfficheListeJoueursSurScores( ArrayList<Joueur> joueurs){
		  
		  if(joueurs.size()==1){
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");  
		  }
		  else if(joueurs.size()==2){
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");
			  lbl_score_j1.setText(joueurs.get(1).getName().toString()+" : 0");
			  
		  }
		  else if(joueurs.size()==3){
			  
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");
			  lbl_score_j1.setText(joueurs.get(1).getName().toString()+" : 0");
			  lbl_score_j2.setText(joueurs.get(2).getName().toString()+" : 0");
		  }
		  else if(joueurs.size()==4){
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");
			  lbl_score_j1.setText(joueurs.get(1).getName().toString()+" : 0");
			  lbl_score_j2.setText(joueurs.get(2).getName().toString()+" : 0");
			  lbl_score_j3.setText(joueurs.get(3).getName().toString()+" : 0");  	  
		  }
		  else if(joueurs.size()==5){
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");
			  lbl_score_j1.setText(joueurs.get(1).getName().toString()+" : 0");
			  lbl_score_j2.setText(joueurs.get(2).getName().toString()+" : 0");
			  lbl_score_j3.setText(joueurs.get(3).getName().toString()+" : 0");
			  lbl_score_j4.setText(joueurs.get(4).getName().toString()+" : 0");
			 
		  }
		  
		  else if(joueurs.size()==6){
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");
			  lbl_score_j1.setText(joueurs.get(1).getName().toString()+" : 0");
			  lbl_score_j2.setText(joueurs.get(2).getName().toString()+" : 0");
			  lbl_score_j3.setText(joueurs.get(3).getName().toString()+" : 0");
			  lbl_score_j4.setText(joueurs.get(4).getName().toString()+" : 0");
			  lbl_score_j5.setText(joueurs.get(5).getName().toString()+" : 0");
			
			  
		  }
		  else if(joueurs.size()==7){
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");
			  lbl_score_j1.setText(joueurs.get(1).getName().toString()+" : 0");
			  lbl_score_j2.setText(joueurs.get(2).getName().toString()+" : 0");
			  lbl_score_j3.setText(joueurs.get(3).getName().toString()+" : 0");
			  lbl_score_j4.setText(joueurs.get(4).getName().toString()+" : 0");
			  lbl_score_j5.setText(joueurs.get(5).getName().toString()+" : 0");
			  lbl_score_j6.setText(joueurs.get(6).getName().toString()+" : 0");
			  
			  
			  
		  }
		  else if(joueurs.size()==8){
			  
			  lbl_score_j0.setText(joueurs.get(0).getName().toString()+" : 0");
			  lbl_score_j1.setText(joueurs.get(1).getName().toString()+" : 0");
			  lbl_score_j2.setText(joueurs.get(2).getName().toString()+" : 0");
			  lbl_score_j3.setText(joueurs.get(3).getName().toString()+" : 0");
			  lbl_score_j4.setText(joueurs.get(4).getName().toString()+" : 0");
			  lbl_score_j5.setText(joueurs.get(5).getName().toString()+" : 0");
			  lbl_score_j6.setText(joueurs.get(6).getName().toString()+" : 0");
			  lbl_score_j7.setText(joueurs.get(7).getName().toString()+" : 0");

		  }
	
			
	  }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String composant = e.getActionCommand();
		System.out.println("le bouton : "+composant+" a été cliqué");
		
		// permet de déselectionner les cartes qui ne sont pas cliqué 
		for(int i=0;i<btn_cartes_du_joueur.length;i++){
		btn_cartes_du_joueur[i].setBorder( new LineBorder(Color.RED, 0, true));	

	
		
		} 
			 String str = e.getActionCommand( ); 
			 JButton btn = (JButton) e.getSource( ); 
			 btn.setBorder(new LineBorder(Color.RED, 6, true));
			 
	
			 if(composant.charAt(0)=='p'){
			System.out.println("innformation de la carte du plateau séléctionné :");	 
			System.out.println(plateauMain.getCarteSurLaTable().get(Character.getNumericValue(composant.charAt(1))));	 
			   // Cas de la carte selectionné sur le plateau
				 
		// Vérifier si le joueur a au moins sélectionner une carte de sa main avant d'appuyer sur une carte du plateau
				  
		if(lbl_nom_carte_joueur_selectionne.getText()=="vide"){
		 btn.setBorder(new LineBorder(Color.RED, 0, true));	
		JOptionPane.showMessageDialog(null, "Vous devez au moins séléctionner une carte dans votre main avant de la placer sur le plateau", "Erreur!", JOptionPane.ERROR_MESSAGE);	
			
		}else{
			boolean estAvant;
			 int dialogButton;
			  dialogButton = JOptionPane.YES_NO_OPTION;
			  lbl_nom_carte_clique_plateau.setText("Nom de la carte cliqué sur le plateau : "+plateauMain.getCarteSurLaTable().get(Character.getNumericValue(composant.charAt(1))).getNom());
				  int dialogResult = JOptionPane.showConfirmDialog (null, "Etes vous sûr de vouloir séléctionner cette carte ? ","Confirmation",dialogButton);
					if(dialogResult == JOptionPane.YES_OPTION){
						 lbl_nom_carte_plateau_selectionne.setText(composant);	
			
						  String[] buttons = { "Après", "Avant" };

						    int rc = JOptionPane.showOptionDialog(null, "Voulez-vous placer la carte avant ou après ?", "Votre choix :)",
						        JOptionPane.INFORMATION_MESSAGE, 0, null, buttons, buttons[1]);
						    System.out.println("-----------------TEST----------");   
						 System.out.println(rc);
						 
	int indexCarteChoisie;
	indexCarteChoisie=Integer.parseInt(lbl_nom_carte_joueur_selectionne.getText());
	int indexEmplacementPlateau;
	// si il y a moins de 10 carte sur le plateau 
	if(lbl_nom_carte_plateau_selectionne.getText().length()<3){
	
	indexEmplacementPlateau=Character.getNumericValue(lbl_nom_carte_plateau_selectionne.getText().charAt(1));
	}else{
		// si il y a plus de 10 carte sur le plateau 		
String valeur=String.valueOf(lbl_nom_carte_plateau_selectionne.getText().charAt(1)+lbl_nom_carte_plateau_selectionne.getText().charAt(2));
indexEmplacementPlateau=Integer.parseInt(valeur);	
	}
	
	boolean resultat;					 
	
	
	
	System.out.println("CARTES EN MAIN AVANT");
	System.out.println((listesDesJoueurs.get(changerJoueur).getCartesEnMain()));
						// si il choisit après 
						if(rc==0){					
// 1)supprimer le bouton de la carte cliqué dans la main du joueur
// 2) la placer sur le plateau	
estAvant=false;


resultat = methoderegletimeline.BoolPlacerCarteJoueurSurPlateauAuBonEndroit(listesDesJoueurs.get(changerJoueur), indexCarteChoisie, plateauMain, indexEmplacementPlateau, estAvant);
if (resultat==true){
System.out.println("TU PIOCHES PAS DE CARTE");
System.out.println("CARTES EN MAIN APRES");
System.out.println(listesDesJoueurs.get(changerJoueur).getCartesEnMain());

// on appel a la fonction Supprimer... pour suppr tt les boutons sur le plateau et ensuite rafficher la liste des cartes à jour !

SupprimerCartesDuJoueur();

//pnl_cartes_joueur.revalidate();
//pnl_cartes_joueur.repaint();
AfficheCartesDuJoueurX(listesDesJoueurs.get(changerJoueur));
AffichePremiereCarteSurleTerrain2(plateauMain);
}else{
	// rafraichir la vue de la main du joueur 
	pnl_cartes_joueur.revalidate();
	pnl_cartes_joueur.repaint();
	
	System.out.println("TU PIOCHES UNE CARTE");	
	System.out.println("ETAT DU PLATEAU :");
	System.out.println(plateauMain.getCarteSurLaTable());
	// Faire piocher le joueur !
	JOptionPane.showMessageDialog(null, "Mauvais Placement de la carte ! Tu pioches ! ");
	methoddeck.PiocherUneCarte(listesDesJoueurs.get(changerJoueur), deckMain);
	
	// rafraichir la vue de la main du joueur 
	pnl_cartes_joueur.revalidate();
	pnl_cartes_joueur.repaint();
}

//ERROR



						}
						// si il choisit avant
						else if (rc==1){						
//supprimer le bouton de la carte cliqué dans la main du joueur								
// 2) la placer sur le plateau
estAvant=true;

resultat = methoderegletimeline.BoolPlacerCarteJoueurSurPlateauAuBonEndroit(listesDesJoueurs.get(changerJoueur), indexCarteChoisie, plateauMain, indexEmplacementPlateau, estAvant);									

if (resultat==true){
//	System.out.println("TU PIOCHES PAS DE CARTE");
//	System.out.println("CARTES EN MAIN APRES");
//	System.out.println(listesDesJoueurs.get(changerJoueur).getCartesEnMain());
//	SupprimerCartesDuJoueur();
//	AfficheCartesDuJoueurX(listesDesJoueurs.get(changerJoueur));
//	AffichePremiereCarteSurleTerrain2(plateauMain);
	
System.out.println("ETAT DU PLATEAU :");
System.out.println(plateauMain.getCarteSurLaTable());

	test=plateauMain.getCarteSurLaTable().size()-1;
	SupprimerCartesDuJoueur();

	AfficheCartesDuJoueurX(listesDesJoueurs.get(changerJoueur));
	
	
	/*
	String chemin="data/timeline/cards/";
	System.out.println(img_cartes_du_plateau[0].getImage());
	img_cartes_du_plateau[0]=new ImageIcon(chemin+listesDesJoueurs.get(changerJoueur).getCartesEnMain().get(indexCarteChoisie).getNom_de_l_image()+".jpeg");
	imga_cartes_du_plateau[0]= img_cartes_du_plateau[0].getImage();
	Image c0;
	c0=imga_cartes_du_plateau[0].getScaledInstance(120, 180,  java.awt.Image.SCALE_SMOOTH);
	img_cartes_du_plateau[0]=new ImageIcon(c0);
	pnl_deck.revalidate();
	pnl_deck.repaint();
	*/

SupprimerCartesDuPlateau();
AffichePremiereCarteSurleTerrain2(plateauMain);		

//AffichePremiereCarteSurleTerrain2(plateauMain);		


//TTESTTTTTT	
//	AffichePremiereCarteSurleTerrain2(plateauMain);
		
}else{
	// rafraichir la vue de la main du joueur 
	pnl_cartes_joueur.revalidate();
	pnl_cartes_joueur.repaint();
	
	System.out.println("TU PIOCHES UNE CARTE");	
	System.out.println("ETAT DU PLATEAU :");
	System.out.println(plateauMain.getCarteSurLaTable());
	// Faire piocher le joueur !
	JOptionPane.showMessageDialog(null, "Mauvais Placement de la carte ! Tu pioches ! ");
	methoddeck.PiocherUneCarte(listesDesJoueurs.get(changerJoueur), deckMain);
	
	// rafraichir la vue de la main du joueur 
	pnl_cartes_joueur.revalidate();
	pnl_cartes_joueur.repaint();
	
	
	

}

						}
						
else{
					
	
	
						}
						 
						 
						}else{
						// cas si on veut choisir une autre carte de sa main	
						// on enleve la bordure rouge autour de la carte et on réinitialise lbl contenant la carte cliqué à vide
							 btn.setBorder(new LineBorder(Color.RED, 0, true));
							 lbl_nom_carte_plateau_selectionne.setText("vide");
							 pnl_cartes_joueur.revalidate();
							pnl_cartes_joueur.repaint();
							return;
						}

// permet de faire passer le tour au prochain joueur					
changerJoueur++;						
if(changerJoueur>=listesDesJoueurs.size()){
changerJoueur=0;
numeroDuTour++;
lbl_numro_du_tour.setText("Numéro du tour : "+numeroDuTour);
}
											
					
					lbl_nom_joueur_en_cours.setText((listesDesJoueurs.get(changerJoueur).getName()));
					SupprimerCartesDuJoueur();
					AfficheCartesDuJoueurX(listesDesJoueurs.get(changerJoueur));
					System.out.println("--------------TEST JOUEUR SUIVANT---------------");
					System.out.println((listesDesJoueurs.get(changerJoueur).getCartesEnMain()));					
				
			 }
			 
			 System.out.println("----LBL NOM associé à la carte cliqué dans la main du joueur---");
			 System.out.println(lbl_nom_carte_joueur_selectionne.getText());
			 System.out.println("----LBL NOM associé à la carte cliqué sur le plateau ---");
			 System.out.println(lbl_nom_carte_plateau_selectionne.getText());
			
				 
	}else{
		
	lbl_nom_carte_joueur_selectionne.setText(composant);
	System.out.println("information de la carte en main cliqué :");
	System.out.println(listesDesJoueurs.get(changerJoueur).getCartesEnMain().get(Integer.parseInt(composant)));
	}
	}
}
	
	

